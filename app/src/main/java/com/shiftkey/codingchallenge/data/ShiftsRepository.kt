package com.shiftkey.codingchallenge.data;

import com.shiftkey.codingchallenge.api.ShiftsService
import com.shiftkey.codingchallenge.data.model.AvailableShiftsResponse
import com.shiftkey.codingchallenge.data.model.Type
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ShiftsRepository @Inject constructor(

    private val shiftService: ShiftsService

) {
    suspend fun getShifts(start: String, type: Type, address: String, radius: Int)
            : Response<AvailableShiftsResponse> {

        return shiftService.getAvailableShifts(start, type.str, address, radius)
    }
}
