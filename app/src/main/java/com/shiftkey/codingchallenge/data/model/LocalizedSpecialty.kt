package com.shiftkey.codingchallenge.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class LocalizedSpecialty(
	@SerializedName("id") val id: Int,
	@SerializedName("specialty_id") val specialty_id: Int,
	@SerializedName("state_id") val state_id: Int,
	@SerializedName("name") val name: String?,
	@SerializedName("abbreviation") val abbreviation: String?,
	@SerializedName("specialty") val specialty: Specialty?
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readInt(),
		parcel.readInt(),
		parcel.readInt(),
		parcel.readString(),
		parcel.readString(),
		parcel.readParcelable(Specialty::class.java.classLoader)
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeInt(id)
		parcel.writeInt(specialty_id)
		parcel.writeInt(state_id)
		parcel.writeString(name)
		parcel.writeString(abbreviation)
		parcel.writeParcelable(specialty, flags)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<LocalizedSpecialty> {
		override fun createFromParcel(parcel: Parcel): LocalizedSpecialty {
			return LocalizedSpecialty(parcel)
		}

		override fun newArray(size: Int): Array<LocalizedSpecialty?> {
			return arrayOfNulls(size)
		}
	}
}