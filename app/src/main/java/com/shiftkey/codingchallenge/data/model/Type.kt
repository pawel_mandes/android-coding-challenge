package com.shiftkey.codingchallenge.data.model

enum class Type(val str: String, val days: Int) {
    WEEK("week", 7),
    FOUR_DAY("4day", 4)
}