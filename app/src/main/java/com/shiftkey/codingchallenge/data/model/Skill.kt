package com.shiftkey.codingchallenge.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Skill(
	@SerializedName("id") val id: Int,
	@SerializedName("name") val name: String?,
	@SerializedName("color") val color: String?
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readInt(),
		parcel.readString(),
		parcel.readString()
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeInt(id)
		parcel.writeString(name)
		parcel.writeString(color)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Skill> {
		override fun createFromParcel(parcel: Parcel): Skill {
			return Skill(parcel)
		}

		override fun newArray(size: Int): Array<Skill?> {
			return arrayOfNulls(size)
		}
	}
}