package com.shiftkey.codingchallenge.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Shift(
	@SerializedName("shift_id") val shift_id: Int,
	@SerializedName("start_time") val start_time: String?,
	@SerializedName("end_time") val end_time: String?,
	@SerializedName("normalized_start_date_time") val normalized_start_date_time: String?,
	@SerializedName("normalized_end_date_time") val normalized_end_date_time: String?,
	@SerializedName("timezone") val timezone: String?,
	@SerializedName("premium_rate") val premium_rate: Boolean,
	@SerializedName("covid") val covid: Boolean,
	@SerializedName("shift_kind") val shift_kind: String?,
	@SerializedName("within_distance") val within_distance: Int,
	@SerializedName("facility_type") val facility_type: FacilityType?,
	@SerializedName("skill") val skill: Skill?,
	@SerializedName("localized_specialty") val localized_specialty: LocalizedSpecialty?
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readInt(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readByte() != 0.toByte(),
		parcel.readByte() != 0.toByte(),
		parcel.readString(),
		parcel.readInt(),
		parcel.readParcelable(FacilityType::class.java.classLoader),
		parcel.readParcelable(Skill::class.java.classLoader),
		parcel.readParcelable(LocalizedSpecialty::class.java.classLoader)
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeInt(shift_id)
		parcel.writeString(start_time)
		parcel.writeString(end_time)
		parcel.writeString(normalized_start_date_time)
		parcel.writeString(normalized_end_date_time)
		parcel.writeString(timezone)
		parcel.writeByte(if (premium_rate) 1 else 0)
		parcel.writeByte(if (covid) 1 else 0)
		parcel.writeString(shift_kind)
		parcel.writeInt(within_distance)
		parcel.writeParcelable(facility_type, flags)
		parcel.writeParcelable(skill, flags)
		parcel.writeParcelable(localized_specialty, flags)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Shift> {
		override fun createFromParcel(parcel: Parcel): Shift {
			return Shift(parcel)
		}

		override fun newArray(size: Int): Array<Shift?> {
			return arrayOfNulls(size)
		}
	}
}