package com.shiftkey.codingchallenge.data.model

import com.google.gson.annotations.SerializedName

data class Data (

	@SerializedName("date") val date : String,
	@SerializedName("shifts") val shifts : List<Shift>
)