package com.shiftkey.codingchallenge.data.model

import com.google.gson.annotations.SerializedName

data class AvailableShiftsResponse (
    @field:SerializedName("data") val data: List<Data>,
    @field:SerializedName("links")val links: List<String>,
    @field:SerializedName("meta")val meta: Meta
)