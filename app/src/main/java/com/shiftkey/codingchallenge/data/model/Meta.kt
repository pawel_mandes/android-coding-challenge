package com.shiftkey.codingchallenge.data.model

import com.google.gson.annotations.SerializedName

data class Meta (

	@SerializedName("lat") val lat : Double,
	@SerializedName("lng") val lng : Double
)