package com.shiftkey.codingchallenge.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Specialty(
	@SerializedName("id") val id: Int,
	@SerializedName("name") val name: String?,
	@SerializedName("color") val color: String?,
	@SerializedName("abbreviation") val abbreviation: String?
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readInt(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeInt(id)
		parcel.writeString(name)
		parcel.writeString(color)
		parcel.writeString(abbreviation)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Specialty> {
		override fun createFromParcel(parcel: Parcel): Specialty {
			return Specialty(parcel)
		}

		override fun newArray(size: Int): Array<Specialty?> {
			return arrayOfNulls(size)
		}
	}
}