package com.shiftkey.codingchallenge.ui.shifts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.shiftkey.codingchallenge.R
import dagger.hilt.android.AndroidEntryPoint
import androidx.databinding.DataBindingUtil.setContentView
import com.shiftkey.codingchallenge.databinding.ShiftsActivityBinding


@AndroidEntryPoint
class ShiftsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ShiftsActivityBinding>(this, R.layout.shifts_activity)
    }
}