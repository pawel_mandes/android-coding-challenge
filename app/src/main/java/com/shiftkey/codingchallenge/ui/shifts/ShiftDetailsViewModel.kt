package com.shiftkey.codingchallenge.ui.shifts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.shiftkey.codingchallenge.data.model.Shift
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ShiftDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
): ViewModel() {

    val shift: MutableLiveData<Shift> = MutableLiveData()

    init {
        val shift = savedStateHandle.get<Shift>(SHIFT_SAVED_STATE_KEY)
        shift?.let {
            this.shift.value = it
        }
    }

    companion object {
        const val SHIFT_SAVED_STATE_KEY = "shiftData"
    }
}