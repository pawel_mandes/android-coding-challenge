package com.shiftkey.codingchallenge.ui.shifts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.shiftkey.codingchallenge.data.model.Type
import com.shiftkey.codingchallenge.databinding.ShiftsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class ShiftsFragment : Fragment() {

    private val shiftsViewModel: ShiftsViewModel by viewModels()
    private var loadShiftsJob: Job? = null

    private var radius: Int = 0
    private lateinit var address: String
    private var currentDate: Date = Date()
    private var firstTime: Boolean = true

    private var previousTotal: Int = 0
    private var visibleThreshold: Int = 5
    private var loading: Boolean = true

    private var firstVisibleItem: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int? = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val adapter = ShiftsAdapter()
        val binding = ShiftsFragmentBinding.inflate(inflater, container, false).apply {
            viewModel = shiftsViewModel
            shiftsList.adapter = adapter

            shiftsList.addOnScrollListener(object : OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    this@ShiftsFragment.onScrolled(recyclerView, dx, dy)
                }
            })
        }

        shiftsViewModel.shifts.observe(viewLifecycleOwner) { shifts ->
            adapter.submitList(shifts)
        }

        if (firstTime) {

            val currentDateStr = SimpleDateFormat("yyyy-MM-dd").format(currentDate)
            address = "Dallas, TX"
            radius = 50

            shiftsViewModel.setTopBar(currentDateStr, address, radius)
            loadShifts(currentDateStr, address, radius)
            firstTime = false
        }

        return binding.root
    }

    private fun loadShifts(date: String, address: String, radius: Int) {
        loadShiftsJob?.cancel()
        loadShiftsJob = lifecycleScope.launch {
            shiftsViewModel.loadShifts(date, address, radius)
        }
    }

    private fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

        visibleItemCount = recyclerView.childCount;
        totalItemCount = recyclerView.layoutManager?.itemCount;
        firstVisibleItem =
            (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount!! > previousTotal) {
                loading = false;
                previousTotal = totalItemCount as Int;
            }
        }

        if (!loading && (totalItemCount?.minus(visibleItemCount))!! <= (firstVisibleItem + visibleThreshold)) {
            loading = true;

            val calendar = Calendar.getInstance()
            calendar.time = currentDate
            calendar.add(Calendar.DAY_OF_YEAR, Type.WEEK.days)
            currentDate = calendar.time

            val currentDateStr = SimpleDateFormat("yyyy-MM-dd").format(currentDate)
            loadShifts(currentDateStr, address, radius)
        }
    }
}
