package com.shiftkey.codingchallenge.ui.shifts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.shiftkey.codingchallenge.databinding.ShiftDetailsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShiftDetailsFragment : Fragment() {

    private val shiftDetailsViewModel: ShiftDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = ShiftDetailsFragmentBinding.inflate(inflater, container, false).apply {
            viewModel = shiftDetailsViewModel
        }

        return binding.root
    }
}