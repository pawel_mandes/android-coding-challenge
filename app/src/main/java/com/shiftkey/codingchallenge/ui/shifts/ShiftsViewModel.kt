package com.shiftkey.codingchallenge.ui.shifts

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shiftkey.codingchallenge.data.ShiftsRepository
import com.shiftkey.codingchallenge.data.model.AvailableShiftsResponse
import com.shiftkey.codingchallenge.data.model.Shift
import com.shiftkey.codingchallenge.data.model.Type
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ShiftsViewModel @Inject internal constructor(

    private val repository: ShiftsRepository

) : ViewModel() {

    private val LOG_TAG = ShiftsViewModel::class.qualifiedName

    val start: MutableLiveData<String> = MutableLiveData()
    val address: MutableLiveData<String> = MutableLiveData()
    val radius: MutableLiveData<String> = MutableLiveData()

    var showProgress: ObservableBoolean = ObservableBoolean(false)

    var shifts: MutableLiveData<MutableList<Shift>> = MutableLiveData()

    init {
        shifts.value = ArrayList<Shift>()
    }

    fun setTopBar(date: String, address: String, radius: Int) {
        this.address.value = address
        this.start.value = date
        this.radius.value = radius.toString()
    }

    suspend fun loadShifts(date: String, address: String, radius: Int) {

        showProgress.set(true)

        val response: Response<AvailableShiftsResponse> =
            repository.getShifts(date, Type.WEEK, address, radius)

        if (response.isSuccessful) {

            response.body()?.data?.flatMap { it.shifts }
                ?.forEach { shift ->
                    shifts.value?.add(shift)
                }

            showProgress.set(false)
        } else {
            onError(response.message())
        }
    }

    fun <T> MutableLiveData<T>.notifyObserver() {
        this.value = this.value
    }

    private fun onError(message: String) {
        showProgress.set(false)
        Log.e(LOG_TAG, message)
    }
}