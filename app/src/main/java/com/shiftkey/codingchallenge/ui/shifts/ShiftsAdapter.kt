package com.shiftkey.codingchallenge.ui.shifts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.data.model.Shift
import com.shiftkey.codingchallenge.databinding.ShiftListItemBinding

class ShiftsAdapter : ListAdapter<Shift, RecyclerView.ViewHolder>(ShiftDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DataViewHolder(
            ShiftListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val shift = getItem(position)
        (holder as DataViewHolder).bind(shift)
    }

    class DataViewHolder(

        private val binding: ShiftListItemBinding

    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.setClickListener {
                binding.shiftData?.let { shift ->
                    goToShiftDetails(shift, it)
                }
            }
        }

        private fun goToShiftDetails(shift: Shift, view: View) {
            val bundle = bundleOf(ShiftDetailsViewModel.SHIFT_SAVED_STATE_KEY to shift)
            view.findNavController().navigate(R.id.action_shifts_list_fragment_to_shift_details_fragment, bundle)
        }

        fun bind(item: Shift) {
            binding.apply {
                shiftData = item
                executePendingBindings()
            }
        }
    }
}

private class ShiftDiffCallback : DiffUtil.ItemCallback<Shift>() {

    override fun areItemsTheSame(oldItem: Shift, newItem: Shift): Boolean {
        return oldItem.shift_id == newItem.shift_id
    }

    override fun areContentsTheSame(oldItem: Shift, newItem: Shift): Boolean {
        return oldItem == newItem
    }
}