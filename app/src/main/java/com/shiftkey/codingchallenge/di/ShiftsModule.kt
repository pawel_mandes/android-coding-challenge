package com.shiftkey.codingchallenge.di
import com.shiftkey.codingchallenge.api.ShiftsService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ShiftsModule {

    @Singleton
    @Provides
    fun provideShiftsService(): ShiftsService {
        return ShiftsService.create()
    }
}