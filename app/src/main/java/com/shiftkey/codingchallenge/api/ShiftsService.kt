package com.shiftkey.codingchallenge.api

import com.shiftkey.codingchallenge.data.model.AvailableShiftsResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ShiftsService {

    @GET("available_shifts")
    suspend fun getAvailableShifts(
        @Query("start") query: String,
        @Query("type") type: String,
        @Query("address") address: String,
        @Query("radius") radius: Int,
    ): Response<AvailableShiftsResponse>

    companion object {
        private const val BASE_URL = "https://staging-app.shiftkey.com/api/v2/"

        fun create(): ShiftsService {
            val logger = HttpLoggingInterceptor().apply { level = Level.BASIC }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ShiftsService::class.java)
        }
    }
}